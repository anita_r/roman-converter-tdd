package com.thoughtworks.vapasi;

import java.util.HashMap;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String roman) {
        int i = 0;
        int result = 0;

        while (i < roman.length() && validateRoman(roman)) {
            char letter = roman.charAt(i);
            int number = getValueForCharacter(letter);
            i++;
            if (i == roman.length()) {
                result = result + number;
            } else {
                int nextNumber = getValueForCharacter(roman.charAt(i));
                if (nextNumber > number) {
                    result = result + (nextNumber - number);
                    i++;
                } else {
                    result = result + number;
                }
            }
        }
         return result;
    }

    public boolean validateRoman(String roman){
        if(!roman.isEmpty()){
            return true;
        }
        return false;
    }

    public Integer getValueForCharacter(char roman) {
        return arabic.get(roman);
    }


    public HashMap<Character, Integer> arabic = new HashMap<Character, Integer>() {
        {
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('M', 1000);
            put('C', 100);
        }
    };
}
